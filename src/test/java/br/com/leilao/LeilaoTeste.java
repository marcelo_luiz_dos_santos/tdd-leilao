package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class LeilaoTeste {

    private Lance lance;
    private Usuario usuario;
    private Leilao leilao;

    @Test
    public void testaAdicaoDeUmLance(){
        leilao = new Leilao();

        usuario = new Usuario("Marcelo", 1);
        lance = new Lance(usuario, 100.00);

        List<Lance> lances = leilao.adicionarLance(lance);

        Assertions.assertEquals(true, lances.contains(lance));
    }

}
